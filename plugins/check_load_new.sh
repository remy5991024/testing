#!/bin/bash
#
#  This script will validate load and divide the result by number of CPUs
# Linux /usr/lib/nagios/plugins/check_load_new.sh
# AIX /opt/nagios/libexec/fit-plugins/check_load_new.sh
#
# Get the processor type
#
PTYPE=$(uname -p)
#
# Thresholds for Vmstat
VmstatWarn=95
VmstatCrit=99
# Thresholds for CPU usage
CPUWarn=95
CPUCrit=99
#
# Process according to processor type
#

case ${PTYPE} in
 x86_64)
   # Linux on Intel (cinl09v742)
   #
   # No. of CPUs
  intel=$(nproc | awk '{print $1}')
   #
   # Vmstat r
   # id column should be 100-$15 for linux 100-$16 for aix
   # vmstat 1 3|tail -1|awk '{usage=(100 - $15)} END {print usage}'
  Rvmstat=$(vmstat 1 3|tail -1|awk '{usage=(100 - $15)} END {print usage}')
   #
   # CPU usage in top, only get User and System
  USSYvariable=$(top -n 5 -b |grep Cpu | tail -1 | awk -F'[:,=]' '{ print $2, $3 }' | sed 's/ //g;s/us/ /g;s/sy//g')
  USvariable=$(echo $USSYvariable | awk '{ print $1 }')
  SYvariable=$(echo $USSYvariable | awk '{ print $2 }')
  CPUusage=$(echo "$USvariable $SYvariable" | awk '{print $1+$2}')
   #
   # Return all values
  RC=$?
  ;;

 ppc64le)
   # Linux on IBM Power Little Endian (cinl09v569)
   #
   # No. of CPUs
  intel=$(lparstat -i |grep -i Online |grep -i "Virtual CPUs" | awk -F":" '{print $2}')
   #
   # Vmstat id
  Rvmstat=$(vmstat 1 3|tail -1|awk '{usage=(100 - $15)} END {print usage}')
   #
   # CPU usage in top, only get User and System
  USSYvariable=$(top -n 5 -b |grep Cpu | tail -1 | awk -F'[:,=]' '{ print $2, $3 }' | sed 's/ //g;s/us/ /g;s/sy//g')
  USvariable=$(echo $USSYvariable | awk '{ print $1 }')
  SYvariable=$(echo $USSYvariable | awk '{ print $2 }')
  CPUusage=$(echo "$USvariable $SYvariable" | awk '{print $1+$2}')
   #
   # Return all values
  RC=$?
  ;;

 ppc64)
   # Linux on IBM Power Big Endian (enel08v004)
   #
   # No. of CPUs
  intel=$(lparstat -i |grep -i Online |grep -i "Virtual CPUs" | awk -F":" '{print $2}')
   #
   # Vmstat id
  Rvmstat=$(vmstat 1 3|tail -1|awk '{usage=(100 - $15)} END {print usage}')
   #
   # CPU usage in top, only get User and System
  USSYvariable=$(top -n 5 -b |grep Cpu | tail -1 | awk -F'[:,=]' '{ print $2, $3 }' | sed 's/ //g;s/%us/ /g;s/%sy//g')
  USvariable=$(echo $USSYvariable | awk '{ print $1 }')
  SYvariable=$(echo $USSYvariable | awk '{ print $2 }')
  CPUusage=$(echo "$USvariable $SYvariable" | awk '{print $1+$2}')
   #
   # Return all values
  RC=$?
  ;;

 powerpc)
   # AIX (cina08v214)
   # Linux on IBM Power Big Endian (enel08v004)
   #
   # No. of CPUs
  intel=$(lparstat -i |grep -i Online |grep -i "Virtual CPUs" | awk -F":" '{print $2}')
   #
   # Vmstat
   # vmstat 1 3|tail -1|awk '{usage=(100 - $16)} END {print usage}'
  CPUusage=$(vmstat 1 3|tail -1|awk '{usage=(100 - $16)} END {print usage}')
   #
   # CPU usage in topas, only get User and System
   # topasrec -L -c 2 -s 60 -o /home/itanay/topas.txt
   # (sleep 3; echo q) | topas > topas.txt
   # sar | tail -n 3 |head -n 1
   # Below 2 entries should exist in crontab
   # 0,10,20,30,40,50 * * * * /usr/lib/sa/sa1
   # 59 23 * * * /usr/lib/sa/sa2 -A
  # USSYvariable=$(sar | tail -n 3 |head -n 1 | awk '{ print $2, $3 }')
  USvariable=$(echo $USSYvariable | awk '{ print $1 }'| sed 's/ //g')
  SYvariable=$(echo $USSYvariable | awk '{ print $2 }'| sed 's/ //g')
  Rvmstat=$(echo "$USvariable $SYvariable" | awk '{print $1+$2}')
   #
   # Return all values
  RC=$?
  ;;

 *)
   # Why is this here?
  ;;
esac

# Calculate Result for Icinga; After this | is the way to graph; <output> | 'label'=value[UOM];[warn];[crit];[min];[max]
# https://icinga.com/docs/icinga-2/latest/doc/05-service-monitoring/#performance-data-metrics
#
# Graph Vmstat for information purposes
# old
# echo "$Rvmstat $VmstatWarn $VmstatCrit $intel $vmstatr" | awk '{ print "#CPUs:"$4"   vmstat-a(r):"$5"   Actual_value:"$1" | \047vmstat metrics\047="$1";"$2";"$3";"$2";"$3; }'
#
# Icinga Status with CPU usage
#
echo "$CPUusage $CPUWarn $CPUCrit $intel $USvariable $SYvariable $Rvmstat $VmstatWarn $VmstatCrit" | awk '{ if ( $1 >= $3 )
        {print "CRITICAL - User:"$5" System:"$6" Total:"$1 "| \047CPU Metrics\047="$1";"$2";"$3";"$2";"$3"; \047vmstat metrics\047="$7;
        print "Informative: #CPU:"$4" vmstat(100-id):"$7;
        exit 2;}
        else if ( $1 >= $2 )
        {print "WARNING - User:"$5" System:"$6" Total:"$1 "| \047CPU Metrics\047="$1";"$2";"$3";"$2";"$3"; \047vmstat metrics\047="$7;
        print "Informative: #CPU:"$4" vmstat(100-id):"$7;
        exit 1;}
        else if ( $1 < $2 )
        {print "OK - User:"$5" System:"$6" Total:"$1 "| \047CPU Metrics\047="$1";"$2";"$3";"$2";"$3"; \047vmstat metrics\047="$7;
        print "Informative: #CPU:"$4" vmstat(100-id):"$7;
        exit 0;}
else {print "UNKNOWN - Please validate script is working correctly";
        exit 3;}
}'
